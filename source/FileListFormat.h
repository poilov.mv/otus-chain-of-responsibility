#pragma once

#include <string>
#include <vector>

class FileListFormat {
    FileListFormat() = delete;
    FileListFormat(const FileListFormat&) = delete;
    FileListFormat &operator=(const FileListFormat&) = delete;

public:
    static std::string toString(const std::vector<std::string> &value);
    static std::vector<std::string> fromString(const std::string &value);
};
