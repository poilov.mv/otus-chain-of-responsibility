#pragma once

#include "ILog.h"
#include <stdexcept>


class FileProcessorError
    : public std::runtime_error {
public:
    explicit FileProcessorError(const std::string& _Message);
};


class AbstractFileProcessor {
    AbstractFileProcessor *nextProcessor = nullptr;

    ILog *log;
    std::string targetFilePath;

public:
    AbstractFileProcessor(ILog *log, const std::string &targetFilePath);

    void process(const std::string &filepath);
    void setNext(AbstractFileProcessor *value);

protected:
    virtual std::string lowerSuffix() const = 0;

private:
    std::string readFile(const std::string &filepath);
    void appendFile(const std::string &content, const std::string &filepath);
};
