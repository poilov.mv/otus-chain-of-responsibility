#include "FileListFormat.h"

#include <sstream>
#include <algorithm>


static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

std::string FileListFormat::toString(const std::vector<std::string> &value) {
    std::ostringstream ss;
    for (auto const& item : value) {
        ss << item << std::endl;
    }
    return ss.str();
}

std::vector<std::string> FileListFormat::fromString(const std::string &value) {
    std::vector<std::string> result;

    std::string line;
    std::istringstream ss(value);
    while (std::getline(ss, line)) {
        ltrim(line);
        rtrim(line);
        if (line.empty()) continue;

        result.emplace_back(line);
    }

    return result;
}
