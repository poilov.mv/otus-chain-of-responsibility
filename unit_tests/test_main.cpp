#include "gtest/gtest.h"

#include "main_app.h"
#include "TextFile.h"
#include "FileListCreator.h"
#include "ConsoleCapturer.h"


class TestMain : public ::testing::Test {
protected:
    TextFile listFile;
    TextFile targetFile;
    ConsoleCapturer console;
    FileListCreator fileList;

    TestMain()
        : listFile("list.txt")
        , targetFile("target.txt")
        , fileList(listFile.path()){
        console.capture();
    }

    void TearDown() override {
        for (auto const& filepath : fileList.filePaths()) {
            TextFile(filepath).remove();
        }

        console.restore();
        listFile.remove();
        targetFile.remove();
    }

    int run_main(
        const std::string &listFilePath = std::string(),
        const std::string &targetFilePath = std::string()
    ) {
        std::string pr("program");
        std::string lp(listFilePath);
        std::string tp(targetFilePath);

        int count = 1
            + (!lp.empty() ? 1 : 0)
            + (!tp.empty() ? 1 : 0);

        char *argv[] = { &pr.at(0),
            lp.empty() ? NULL : &lp.at(0),
            tp.empty() ? NULL : &tp.at(0) };
        return main_app(count, argv);
    }
};


TEST_F(TestMain, output_txt_content_to_target) {
    FileListCreator(listFile.path())
        .prepareFile("file1.txt")
        .prepareFile("file2.tXt")
        .prepareFile("file3.TxT")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "file1.txt\nfile2.tXt\nfile3.TxT\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_txt_message_to_console) {
    FileListCreator(listFile.path())
        .prepareFile("file1.txt")
        .prepareFile("file2.tXt")
        .prepareFile("file3.TxT")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    auto resultContent = console.text();
    auto expectedContent =
        "���������� TXT ������� ���� file1.txt\n"
        "���������� TXT ������� ���� file2.tXt\n"
        "���������� TXT ������� ���� file3.TxT\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_csv_content_to_target) {
    FileListCreator(listFile.path())
        .prepareFile("file1.csv")
        .prepareFile("file2.cSv")
        .prepareFile("file3.CSv")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "file1.csv\nfile2.cSv\nfile3.CSv\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_csv_message_to_console) {
    FileListCreator(listFile.path())
        .prepareFile("file1.csv")
        .prepareFile("file2.cSv")
        .prepareFile("file3.CSv")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    auto resultContent = console.text();
    auto expectedContent =
        "���������� CSV ������� ���� file1.csv\n"
        "���������� CSV ������� ���� file2.cSv\n"
        "���������� CSV ������� ���� file3.CSv\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_json_content_to_target) {
    FileListCreator(listFile.path())
        .prepareFile("file1.json")
        .prepareFile("file2.JsoN")
        .prepareFile("file3.JSON")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "file1.json\nfile2.JsoN\nfile3.JSON\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_json_message_to_console) {
    FileListCreator(listFile.path())
        .prepareFile("file1.json")
        .prepareFile("file2.JsoN")
        .prepareFile("file3.JSON")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    auto resultContent = console.text();
    auto expectedContent =
        "���������� JSON ������� ���� file1.json\n"
        "���������� JSON ������� ���� file2.JsoN\n"
        "���������� JSON ������� ���� file3.JSON\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_xml_content_to_target) {
    FileListCreator(listFile.path())
        .prepareFile("file1.xml")
        .prepareFile("file2.XMl")
        .prepareFile("file3.xmL")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "file1.xml\nfile2.XMl\nfile3.xmL\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_xml_message_to_console) {
    FileListCreator(listFile.path())
        .prepareFile("file1.xml")
        .prepareFile("file2.XMl")
        .prepareFile("file3.xmL")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    auto resultContent = console.text();
    auto expectedContent =
        "���������� XML ������� ���� file1.xml\n"
        "���������� XML ������� ���� file2.XMl\n"
        "���������� XML ������� ���� file3.xmL\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, dont_output_any_content_to_target) {
    FileListCreator(listFile.path())
        .prepareFile("file1.any")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    ASSERT_FALSE(targetFile.exists());
}

TEST_F(TestMain, dont_output_any_message_to_console) {
    FileListCreator(listFile.path())
        .prepareFile("file1.any")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    ASSERT_TRUE(console.text().empty());
}

TEST_F(TestMain, output_different_file_types_content_to_target) {
    FileListCreator(listFile.path())
        .prepareFile("file1.xml")
        .prepareFile("file2.any")
        .prepareFile("file3.txt")
        .prepareFile("file4.csv")
        .prepareFile("file5.json")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "file1.xml\nfile3.txt\nfile4.csv\nfile5.json\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, output_different_file_types_message_to_console) {
    FileListCreator(listFile.path())
        .prepareFile("file1.xml")
        .prepareFile("file2.any")
        .prepareFile("file3.txt")
        .prepareFile("file4.csv")
        .prepareFile("file5.json")
        .create();

    ASSERT_EQ(0, run_main(listFile.path(), targetFile.path()));

    auto resultContent = console.text();
    auto expectedContent =
        "���������� XML ������� ���� file1.xml\n"
        "���������� TXT ������� ���� file3.txt\n"
        "���������� CSV ������� ���� file4.csv\n"
        "���������� JSON ������� ���� file5.json\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, show_error_if_file_list_not_exists) {
    ASSERT_EQ(2, run_main(listFile.path(), targetFile.path()));
}

TEST_F(TestMain, show_synopsis_and_returns_error_without_args) {
    ASSERT_EQ(1, run_main());
}
