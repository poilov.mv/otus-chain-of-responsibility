[![pipeline status](https://gitlab.com/poilov.mv/otus-chain-of-responsibility/badges/master/pipeline.svg)](https://gitlab.com/poilov.mv/otus-chain-of-responsibility/-/commits/master)
[![coverage report](https://gitlab.com/poilov.mv/otus-chain-of-responsibility/badges/master/coverage.svg)](https://gitlab.com/poilov.mv/otus-chain-of-responsibility/-/commits/master)

# Шаблоны проектирования

## Домашнее задание по теме **Цепочка ответственности**
**Цель**: Получите навыки применения шаблона "цепочка ответственности"
Программа, реализующая алгоритм, получает на вход список файлов. И каждый попадает в обработку алгоритма.

На вход алгоритма передаётся ряд файлов, которые имеют различный тип (Xml, JSON, CSV, txt)
Требуется создать цепочку обработки этих файлов, где отдельный обработчик отвечает за обработку конкретного типа документа.
Обработчик логирует получение подходящего ему файла в виде "обработчик TXT получил файл filename.txt" и копирует содержимое в выходной файл.

Требуется:
1. создать программу, где на вход подаётся путь файла со списком обрабатываемых файлов и путь выходного файла.
2. реализовать алгоритм обработки с помощью шаблона "Цепочка ответственности"
3. нарисовать диаграмму классов.

## Диаграмма классов
### Наследование
```mermaid
classDiagram
    class AbstractFileProcessor {
      nextProcessor AbstractFileProcessor
      log ILog;
      targetFilePath string

      +AbstractFileProcessor(ILog *log, const std::string &targetFilePath);
      +process(filepath);
      +setNext(processor)
      #lowerSuffix() string virtual
      -readFile(filepath) string;
      -appendFile(content, filepath)
    }

    class CsvFileProcessor {
        #lowerSuffix() string override
    }

    class TxtFileProcessor {
        #lowerSuffix() string override
    }

    class JsonFileProcessor {
        #lowerSuffix() string override
    }

    class XmlFileProcessor {
        #lowerSuffix() string override
    }

    AbstractFileProcessor <|-- CsvFileProcessor
    AbstractFileProcessor <|-- TxtFileProcessor
    AbstractFileProcessor <|-- JsonFileProcessor
    AbstractFileProcessor <|-- XmlFileProcessor
```

```mermaid
  classDiagram

    class ILog {
      +writeLine(string)
    }

    class ConsoleLogger {
      +writeLine(string)
    }

    ILog <|-- ConsoleLogger
```
### Вспомогательные классы
```mermaid
classDiagram
  class ArgumentParser {
    list<string> args
    +isArgsValid() bool
    +operation() string
    +sourceFilePath() string
    +targetFilePath() string
    +synopsis() string
  }

  class TextFile {
    string filepath
    string content

    +TextFile(filepath)
    +path() string
    +suffix() string
    +setContent(value)
    +getContent()
    +read() bool
    +write() bool
    +exists() bool
    +remove()
    +writeContent(content, filepath) static
  }

  class FileListFormat {
    toString(list) string static
    fromString(string) list static
  }
```

## Исходные файлы
### Основное приложение
* source/main.cpp
* source/main_app.h
* source/main_app.cpp
### Абстрактный базовый класс для реализации цепочки ответственности
* source/AbstractFileProcessor.h
* source/AbstractFileProcessor.cpp
### Реализация разных обработчиков файлов
* source/CsvFileProcessor.h
* source/JsonFileProcessor.h
* source/TxtFileProcessor.h
* source/XmlFileProcessor.h
### Интерфейс записи в лог
* source/ILog.h
### Реализация лога в окно консоли
* source/ConsoleLogger.h
### Операции форматирования списка файлов
* source/FileListFormat.h
* source/FileListFormat.cpp
### Операции для упрощения работы со строками
* source/StringExtensions.h
* source/StringExtensions.cpp
### Класс разбора аргументов командной строки
* source/ArgumentParser.h
* source/ArgumentParser.cpp
### Класс обертка для чтения и сохранения текстового файла
* source/TextFile.h
* source/TextFile.cpp
### Тесты
* unit_tests/test_argumentparser.cpp
* unit_tests/test_textfile.cpp
* unit_tests/test_filelistformat.cpp
* unit_tests/test_stringextensions.cpp
* unit_tests/test_abstractfileprocessor.cpp
* unit_tests/test_xmlfileprocessor.cpp
* unit_tests/test_csvfileprocessor.cpp
* unit_tests/test_txtfileprocessor.cpp
* unit_tests/test_jsonfileprocessor.cpp
* unit_tests/test_consolelogger.cpp
* unit_tests/test_main.cpp
### Тесты (вспомогательные классы)
* unit_tests/ConsoleCapturer.h
* unit_tests/ConsoleCapturer.cpp
* unit_tests/FileListCreator.h
* unit_tests/FileListCreator.cpp
* unit_tests/MockFileProcessor.h
* unit_tests/MockLog.h
