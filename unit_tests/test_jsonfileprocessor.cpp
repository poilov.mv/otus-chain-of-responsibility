#include "gtest/gtest.h"

#include "TextFile.h"
#include "StringExtensions.h"
#include "JsonFileProcessor.h"

#include "MockLog.h"


class TestJsonFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;
    TextFile anyFile;
    MockLog logger;

    TestJsonFileProcessor()
        : sourceFile("source.json")
        , targetFile("target.txt")
        , anyFile("source.any") {}

    void SetUp() override {}

    void TearDown() override {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestJsonFileProcessor, is_instance_of_abstract_file_processor) {
    JsonFileProcessor processor(nullptr, "");

    ASSERT_TRUE(static_cast<AbstractFileProcessor*>(&processor));
}

TEST_F(TestJsonFileProcessor, do_process_json_files) {
    sourceFile.write();
    JsonFileProcessor processor(&logger, targetFile.path());

    processor.process(sourceFile.path());

    ASSERT_TRUE(targetFile.exists());
}

TEST_F(TestJsonFileProcessor, do_not_process_not_json_files) {
    anyFile.write();
    JsonFileProcessor processor(&logger, targetFile.path());

    processor.process(anyFile.path());

    ASSERT_FALSE(targetFile.exists());
}
