#pragma once

#include <string>


std::string toLower(const std::string &value);

std::string toUpper(const std::string &value);
