#pragma once

#include "ILog.h"


class MockLog
    : public ILog {
public:
    bool writeLineCalled = false;
    std::string writeLineArg;

    void writeLine(const std::string &value) override {
        writeLineCalled = true;
        writeLineArg = value;
    }
};
