#pragma once

#include <sstream>


class ConsoleCapturer {
    std::stringstream buffer;
    std::streambuf* prevcoutbuf;

public:
    void capture();
    void restore();

    std::string text();
};
