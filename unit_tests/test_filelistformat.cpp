#include "gtest/gtest.h"

#include "FileListFormat.h"


TEST(TestFileListFormat, can_parse_string_with_one_file) {
    auto result = FileListFormat::fromString("any");

    ASSERT_EQ(1, result.size());
    ASSERT_EQ("any", result[0]);
}

TEST(TestFileListFormat, can_parse_empty_string) {
    auto result = FileListFormat::fromString("");

    ASSERT_EQ(0, result.size());
}

TEST(TestFileListFormat, can_parse_string_with_two_files) {
    auto result = FileListFormat::fromString("one\ntwo");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("one", result[0]);
    ASSERT_EQ("two", result[1]);
}

TEST(TestFileListFormat, can_skip_empty_lines) {
    auto result = FileListFormat::fromString("one\n\ntwo");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("one", result[0]);
    ASSERT_EQ("two", result[1]);
}

TEST(TestFileListFormat, trim_space_from_filepath) {
    auto result = FileListFormat::fromString(" one  ");

    ASSERT_EQ(1, result.size());
    ASSERT_EQ("one", result[0]);
}

TEST(TestFileListFormat, can_skip_trailing_empty_lines) {
    auto result = FileListFormat::fromString("one\ntwo\n\n");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("one", result[0]);
    ASSERT_EQ("two", result[1]);
}

TEST(TestFileListFormat, can_skip_leading_empty_lines) {
    auto result = FileListFormat::fromString("\n\none\ntwo");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("one", result[0]);
    ASSERT_EQ("two", result[1]);
}

TEST(TestFileListFormat, can_make_string_from_paths) {
    auto result = FileListFormat::toString({"one", "two", "three"});

    ASSERT_EQ("one\ntwo\nthree\n", result);
}
