#pragma once

#include "AbstractFileProcessor.h"


class TxtFileProcessor
    : public AbstractFileProcessor {
public:
    TxtFileProcessor(ILog *log, const std::string &targetFilePath)
        : AbstractFileProcessor(log, targetFilePath) {}

protected:
    std::string lowerSuffix() const override {
        return "txt";
    }
};
