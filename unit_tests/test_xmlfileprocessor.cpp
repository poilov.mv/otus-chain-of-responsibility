#include "gtest/gtest.h"

#include "TextFile.h"
#include "StringExtensions.h"
#include "XmlFileProcessor.h"

#include "MockLog.h"


class TestXmlFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;
    TextFile anyFile;
    MockLog logger;

    TestXmlFileProcessor()
        : sourceFile("source.xml")
        , targetFile("target.txt")
        , anyFile("source.any") {}

    void SetUp() override {}

    void TearDown() override {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestXmlFileProcessor, is_instance_of_abstract_file_processor) {
    XmlFileProcessor processor(nullptr, "");

    ASSERT_TRUE(static_cast<AbstractFileProcessor*>(&processor));
}

TEST_F(TestXmlFileProcessor, do_process_xml_files) {
    sourceFile.write();
    XmlFileProcessor processor(&logger, targetFile.path());

    processor.process(sourceFile.path());

    ASSERT_TRUE(targetFile.exists());
}

TEST_F(TestXmlFileProcessor, do_not_process_not_xml_files) {
    anyFile.write();
    XmlFileProcessor processor(&logger, targetFile.path());

    processor.process(anyFile.path());

    ASSERT_FALSE(targetFile.exists());
}
