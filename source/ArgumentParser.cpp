#include "ArgumentParser.h"

#include <sstream>


ArgumentParser::ArgumentParser(int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        args.push_back(argv[i]);
    }
}

bool ArgumentParser::isArgsValid() const {
    return args.size() == 2;
}

std::string ArgumentParser::synopsis() const {
    std::stringstream ss;
    ss << "Usage:" << std::endl;
    ss << "chain.exe <file_with_list> <output_file>" << std::endl;
    return ss.str();
}

std::string ArgumentParser::fileListPath() const {
    return args[0];
}

std::string ArgumentParser::targetFilePath() const {
    return args[1];
}
