#include "gtest/gtest.h"

#include "TextFile.h"
#include "StringExtensions.h"
#include "CsvFileProcessor.h"

#include "MockLog.h"


class TestCsvFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;
    TextFile anyFile;
    MockLog logger;

    TestCsvFileProcessor()
        : sourceFile("source.csv")
        , targetFile("target.txt")
        , anyFile("source.any") {}

    void SetUp() override {}

    void TearDown() override {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestCsvFileProcessor, is_instance_of_abstract_file_processor) {
    CsvFileProcessor processor(nullptr, "");

    ASSERT_TRUE(static_cast<AbstractFileProcessor*>(&processor));
}

TEST_F(TestCsvFileProcessor, do_process_csv_files) {
    sourceFile.write();
    CsvFileProcessor processor(&logger, targetFile.path());

    processor.process(sourceFile.path());

    ASSERT_TRUE(targetFile.exists());
}

TEST_F(TestCsvFileProcessor, do_not_process_not_csv_files) {
    anyFile.write();
    CsvFileProcessor processor(&logger, targetFile.path());

    processor.process(anyFile.path());

    ASSERT_FALSE(targetFile.exists());
}
