#pragma once

#include "AbstractFileProcessor.h"


class JsonFileProcessor
    : public AbstractFileProcessor {
public:
    JsonFileProcessor(ILog *log, const std::string &targetFilePath)
        : AbstractFileProcessor(log, targetFilePath) {}

protected:
    std::string lowerSuffix() const override {
        return "json";
    }
};
