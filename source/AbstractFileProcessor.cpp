#include "AbstractFileProcessor.h"

#include "StringExtensions.h"
#include "TextFile.h"


FileProcessorError::FileProcessorError(const std::string& _Message)
    : std::runtime_error(_Message) {}


AbstractFileProcessor::AbstractFileProcessor(ILog *log, const std::string &targetFilePath)
    : log(log)
    , targetFilePath(targetFilePath) {}

void AbstractFileProcessor::process(const std::string &filepath) {
    auto suffix = toLower(TextFile(filepath).suffix());
    if (suffix == lowerSuffix()) {
        log->writeLine("���������� " + toUpper(lowerSuffix())
            + " ������� ���� " + filepath);

        auto content = readFile(filepath);
        appendFile(content, targetFilePath);
    } else {
        if (nextProcessor) {
            nextProcessor->process(filepath);
        }
    }
}

void AbstractFileProcessor::setNext(AbstractFileProcessor *value) {
    nextProcessor = value;
}

std::string AbstractFileProcessor::readFile(const std::string &filepath) {
    TextFile file(filepath);
    if (!file.read()) {
        throw FileProcessorError("Can not read file: " + filepath);
    }
    return file.getContent();
}

void AbstractFileProcessor::appendFile(const std::string &content, const std::string &filepath) {
    TextFile file(filepath);
    file.read();

    file.setContent(file.getContent() + content);

    if (!file.write()) {
        throw FileProcessorError("Can not write file: " + filepath);
    }
}
