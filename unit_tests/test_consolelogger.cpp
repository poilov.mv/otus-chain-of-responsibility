#include "gtest/gtest.h"

#include "ConsoleLogger.h"
#include "ConsoleCapturer.h"


class TestConsoleLogger : public ::testing::Test {
protected:
    ConsoleCapturer console;

    void SetUp() {
        console.capture();
    }

    void TearDown() override {
        console.restore();
    }
};


TEST_F(TestConsoleLogger, can_log_to_console) {
    ConsoleLogger logger;

    logger.writeLine("any string");

    ASSERT_EQ("any string\n", console.text());
}

TEST_F(TestConsoleLogger, can_log_another_string_to_console) {
    ConsoleLogger logger;

    logger.writeLine("another");

    ASSERT_EQ("another\n", console.text());
}
