#include "ConsoleCapturer.h"

#include <iostream>


void ConsoleCapturer::capture() {
    std::streambuf* prevcoutbuf = std::cout.rdbuf(buffer.rdbuf());
}

void ConsoleCapturer::restore() {
    std::cout.rdbuf(prevcoutbuf);
}

std::string ConsoleCapturer::text() {
    return buffer.str();
}
