#pragma once

#include "AbstractFileProcessor.h"


class XmlFileProcessor
    : public AbstractFileProcessor {
public:
    XmlFileProcessor(ILog *log, const std::string &targetFilePath)
        : AbstractFileProcessor(log, targetFilePath) {}

protected:
    std::string lowerSuffix() const override {
        return "xml";
    }
};
