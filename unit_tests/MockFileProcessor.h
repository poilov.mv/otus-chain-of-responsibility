#pragma once

#include "AbstractFileProcessor.h"


class MockFileProcessor
    : public AbstractFileProcessor {
public:
    MockFileProcessor(ILog *log, const std::string &targetFilePath)
        : AbstractFileProcessor(log, targetFilePath) {}

    mutable bool lowerSuffixCalled = false;
    std::string lowerSuffixResult;

protected:
    std::string lowerSuffix() const override {
        lowerSuffixCalled = true;
        return lowerSuffixResult;
    }
};
