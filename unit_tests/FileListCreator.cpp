#include "FileListCreator.h"

#include "TextFile.h"
#include "FileListFormat.h"


FileListCreator::FileListCreator(const std::string &filepath)
    : listfilepath(filepath) {}

FileListCreator &FileListCreator::prepareFile(const std::string &filepath) {
    files.push_back(filepath);
    TextFile::writeContent(filepath + "\n", filepath);
    return *this;
}

void FileListCreator::create() {
    TextFile::writeContent(FileListFormat::toString(files), listfilepath);
}

std::vector<std::string> FileListCreator::filePaths() const {
    return files;
}
