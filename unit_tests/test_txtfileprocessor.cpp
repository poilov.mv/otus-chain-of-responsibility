#include "gtest/gtest.h"

#include "TextFile.h"
#include "StringExtensions.h"
#include "TxtFileProcessor.h"

#include "MockLog.h"


class TestTxtFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;
    TextFile anyFile;
    MockLog logger;

    TestTxtFileProcessor()
        : sourceFile("source.txt")
        , targetFile("target.txt")
        , anyFile("source.any") {}

    void SetUp() override {}

    void TearDown() override {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestTxtFileProcessor, is_instance_of_abstract_file_processor) {
    TxtFileProcessor processor(nullptr, "");

    ASSERT_TRUE(static_cast<AbstractFileProcessor*>(&processor));
}

TEST_F(TestTxtFileProcessor, do_process_txt_files) {
    sourceFile.write();
    TxtFileProcessor processor(&logger, targetFile.path());

    processor.process(sourceFile.path());

    ASSERT_TRUE(targetFile.exists());
}

TEST_F(TestTxtFileProcessor, do_not_process_not_txt_files) {
    anyFile.write();
    TxtFileProcessor processor(&logger, targetFile.path());

    processor.process(anyFile.path());

    ASSERT_FALSE(targetFile.exists());
}
