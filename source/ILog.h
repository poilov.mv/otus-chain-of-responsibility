#pragma once

#include <string>


class ILog {
public:
    virtual ~ILog() = default;

    virtual void writeLine(const std::string &value) = 0;
};
