#include "gtest/gtest.h"

#include "TextFile.h"
#include "StringExtensions.h"
#include "AbstractFileProcessor.h"

#include "MockLog.h"
#include "MockFileProcessor.h"

class FakeFileProcessor
    : public AbstractFileProcessor {
public:
    FakeFileProcessor(ILog *log, const std::string &targetFilePath)
        : AbstractFileProcessor(log, targetFilePath) {}

protected:
    std::string lowerSuffix() const override {
        return "tst";
    }
};

class TestAbstractFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;
    MockLog logger;
    FakeFileProcessor processor;

    TestAbstractFileProcessor()
        : sourceFile("source.tst")
        , targetFile("target.txt")
        , processor(&logger, targetFile.path()) {}

    void SetUp() override {}

    void TearDown() override {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestAbstractFileProcessor, stop_process_on_tst_file) {
    sourceFile.write();
    MockFileProcessor next(&logger, targetFile.path());
    processor.setNext(&next);

    processor.process(sourceFile.path());

    ASSERT_FALSE(next.lowerSuffixCalled);
}

TEST_F(TestAbstractFileProcessor, do_nothing_if_has_no_next_processor) {
    processor.process("source.any");

    ASSERT_FALSE(targetFile.exists());
}

TEST_F(TestAbstractFileProcessor, call_next_process_on_any_other_then_tst_flie) {
    TextFile anyFile("source.any");
    anyFile.write();
    MockFileProcessor next(&logger, targetFile.path());
    processor.setNext(&next);

    processor.process(anyFile.path());

    ASSERT_TRUE(next.lowerSuffixCalled);
    anyFile.remove();
}

TEST_F(TestAbstractFileProcessor, write_log_on_tst_file) {
    sourceFile.write();

    processor.process(sourceFile.path());

    ASSERT_EQ("���������� TST ������� ���� " + sourceFile.path(), logger.writeLineArg);
}

TEST_F(TestAbstractFileProcessor, process_case_insensitive) {
    sourceFile = TextFile("source.TsT");
    sourceFile.write();

    processor.process(sourceFile.path());

    ASSERT_FALSE(logger.writeLineArg.empty());
}

TEST_F(TestAbstractFileProcessor, write_content_of_source_to_target) {
    sourceFile.setContent("some content");
    sourceFile.write();

    processor.process(sourceFile.path());

    targetFile.read();
    ASSERT_EQ("some content", targetFile.getContent());
}

TEST_F(TestAbstractFileProcessor, append_content_of_source_to_target) {
    sourceFile.setContent("some content");
    sourceFile.write();
    targetFile.setContent("existing content\n");
    targetFile.write();

    processor.process(sourceFile.path());

    targetFile.read();
    ASSERT_EQ("existing content\nsome content", targetFile.getContent());
}

TEST_F(TestAbstractFileProcessor, raises_error_on_wrong_source_file) {
    ASSERT_THROW({
        processor.process(sourceFile.path());
        }, FileProcessorError);
}

TEST_F(TestAbstractFileProcessor, raises_error_on_wrong_target_file_path) {
    sourceFile.write();
    processor = FakeFileProcessor(&logger, "wrong_file/?*.any");

    ASSERT_THROW({
        processor.process(sourceFile.path());
        }, FileProcessorError);
}
