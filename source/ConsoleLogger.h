#pragma once

#include "ILog.h"
#include <iostream>


class ConsoleLogger
    : public ILog {
public:
    void writeLine(const std::string &value) override {
        std::cout << value << std::endl;
    }
};
