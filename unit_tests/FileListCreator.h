#pragma once

#include <string>
#include <vector>


class FileListCreator {
    std::string listfilepath;
    std::vector<std::string> files;

public:
    FileListCreator(const std::string &filepath);

    FileListCreator &prepareFile(const std::string &filepath);

    void create();

    std::vector<std::string> filePaths() const;
};