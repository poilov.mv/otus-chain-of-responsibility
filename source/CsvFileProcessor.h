#pragma once

#include "AbstractFileProcessor.h"


class CsvFileProcessor
    : public AbstractFileProcessor {
public:
    CsvFileProcessor(ILog *log, const std::string &targetFilePath)
        : AbstractFileProcessor(log, targetFilePath) {}

protected:
    std::string lowerSuffix() const override {
        return "csv";
    }
};
