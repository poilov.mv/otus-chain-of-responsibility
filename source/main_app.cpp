#include "main_app.h"

#include "ArgumentParser.h"
#include "TextFile.h"
#include "FileListFormat.h"
#include "XmlFileProcessor.h"
#include "JsonFileProcessor.h"
#include "TxtFileProcessor.h"
#include "CsvFileProcessor.h"
#include "ConsoleLogger.h"

#include <iostream>


int main_app(int argc, char **argv) {
    ConsoleLogger logger;
    auto args = ArgumentParser(argc, argv);
    if (args.isArgsValid()) {
        TextFile fileList(args.fileListPath());
        if (!fileList.read()) {
            logger.writeLine("Can not read list of files: " + args.fileListPath());
            return 2;
        }

        XmlFileProcessor xml(&logger, args.targetFilePath());
        JsonFileProcessor json(&logger, args.targetFilePath());
        CsvFileProcessor csv(&logger, args.targetFilePath());
        TxtFileProcessor txt(&logger, args.targetFilePath());
        
        xml.setNext(&json);
        json.setNext(&csv);
        csv.setNext(&txt);

        auto fileProcessor = &xml;

        auto files = FileListFormat::fromString(fileList.getContent());
        for (auto const& filePath : files) {
            fileProcessor->process(filePath);
        }

        return 0;
    } else {
        logger.writeLine(args.synopsis());
        return 1;
    }
}
