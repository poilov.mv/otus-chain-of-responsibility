#include "gtest/gtest.h"

#include "ArgumentParser.h"


class TestArgumentParser : public ::testing::Test {
protected:
    ArgumentParser createParser(
        const std::string &arg1 = std::string(),
        const std::string &arg2 = std::string()
    ) {
        std::string p("program");
        std::string a1(arg1);
        std::string a2(arg2);

        int count = 1
            + (!a1.empty() ? 1 : 0)
            + (!a2.empty() ? 1 : 0);

        char *argv[] = { &p.at(0),
            a1.empty() ? NULL : &a1.at(0),
            a2.empty() ? NULL : &a2.at(0) };

        return ArgumentParser(count, argv);
    }
};

TEST_F(TestArgumentParser, can_parse_two_args) {
    auto parser = createParser("filelist.txt", "output");

    ASSERT_TRUE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, cant_parse_not_two_args) {
    auto parser = createParser("5");

    ASSERT_FALSE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, can_get_filelist_path) {
    auto parser = createParser("filelist.txt", "output");

    ASSERT_EQ(std::string("filelist.txt"), parser.fileListPath());
}

TEST_F(TestArgumentParser, can_get_targetfile_path) {
    auto parser = createParser("filelist.txt", "output");

    ASSERT_EQ(std::string("output"), parser.targetFilePath());
}

TEST_F(TestArgumentParser, can_get_synopsis) {
    auto parser = createParser();

    auto synopsis = parser.synopsis();

    ASSERT_FALSE(synopsis.empty());
}
