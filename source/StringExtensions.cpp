#include "StringExtensions.h"

#include <algorithm>


std::string toLower(const std::string &value) {
    auto result = value;
    std::transform(result.begin(), result.end(), result.begin(), [](std::string::value_type value) {
        return std::tolower(value);
    });
    return result;
}

std::string toUpper(const std::string &value) {
    auto result = value;
    std::transform(result.begin(), result.end(), result.begin(), [](std::string::value_type value) {
        return std::toupper(value);
    });
    return result;
}
