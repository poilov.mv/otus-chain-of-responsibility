#include "gtest/gtest.h"

#include "StringExtensions.h"


TEST(TestStringExtensions, test_case_insensitive_string_to_lower) {
    ASSERT_EQ("some string !ab", toLower("SoMe sTriNg !ab"));
}

TEST(TestStringExtensions, test_case_insensitive_string_to_upper) {
    ASSERT_EQ("SOME STRING !AB", toUpper("SoMe sTriNg !ab"));
}
